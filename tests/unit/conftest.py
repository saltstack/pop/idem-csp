from typing import List
from unittest import mock

import pytest


# ================================================================================
# pop fixtures
# ================================================================================
@pytest.fixture(scope="session", autouse=True)
def acct_subs() -> List[str]:
    return ["R__CLOUD__"]


@pytest.fixture(scope="session", autouse=True)
def acct_profile() -> str:
    # The name of the profile that will be used for tests
    # Do NOT use production credentials for tests!
    # A profile with this name needs to be defined in the $ACCT_FILE in order to run these tests
    return "test_development_idem_R__CLOUD__"


@pytest.fixture
def hub(hub):
    hub.pop.sub.add(dyne_name="idem")
    hub.OPT = mock.MagicMock()
    return hub


@pytest.fixture
def ctx():
    """
    Override pytest-pop's ctx fixture with one that has a mocked session
    """
    yield data.NamespaceDict(
        acct=data.NamespaceDict(
            token=data.NamespaceDict(acces_token="mock_token"),
            csp_url="mock_csp_url",
            org_id="mock_org_id",
        )
    )
